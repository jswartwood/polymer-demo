/**
 * isSelf
 *
 * @module      :: Policy
 * @description :: Simple policy to allow only the current player to access itself
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {
  var self = req.session.passport.user,
      id = req.param('id');

  // Current player only
  if (self && (!id || self == id)) {
    req.body.id = self;
    return next();
  }

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)
  return res.forbidden(res.__('forbidden'));
};
