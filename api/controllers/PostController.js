var path = require('path');

module.exports = {
  list: function(req, res, next) {
    res.json(require(path.join(process.cwd(), 'data/posts.json')));
  }
};
