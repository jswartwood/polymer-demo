Deckster API
==========

The Deckster API is a platform for Magic the Gathering. It offers RESTful
resources for players, decks, and cards.

...
