var passport = require('passport'),
    GitHubStrategy = require('passport-github').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    TwitterStrategy = require('passport-twitter').Strategy;



function verifyHandler(token, tokenSecret, profile, done) {
  process.nextTick(function () {
    var uidKey = profile.provider + '_uid',
        email = profile.emails && profile.emails[0] && profile.emails[0].value,
        query = {};

    query[uidKey] = profile.id;

    if (!email) {
      log.error('No email from provider!');
      log.debug('profile', profile);
    }

    // Shouldn't we always have email?
    if (email) {
      query = {or: [
        // Favor email; should reduce dupe email criticalities!
        {email: email},
        query
      ]};
    }

    Player.findOne(query).done(function (err, player) {
      var data = {
            name: profile.displayName,
            email: email
          };
      data[uidKey] = profile.id;

      if (player) return updatePlayer(player, data, done);

      Player.create(data, done);
    });
  });
};

function updatePlayer(player, data, done) {
  var requiresUpdate = false;
  Object.keys(data).forEach(function (key) {
    if (player[key] != data[key]) {
      requiresUpdate = true;
      player[key] = data[key];
    }
  });

  if (requiresUpdate) {
    player.save(function (err) {
      done(err, player);
    });
  } else {
    done(null, player);
  }
}

passport.serializeUser(function (player, done) {
  done(null, player.id);
});

passport.deserializeUser(function (id, done) {
  Player.findOne({id: id}, function (err, player) {
    done(err, player);
  });
});

function hsts(req, res, next) {
  res.setHeader('Strict-Transport-Security', 'max-age=31536000');
  next();
}

module.exports.express = {
  customMiddleware: function (app) {
    passport.use(new GitHubStrategy(
      {
        clientID: '524f7ef5c79a52262000',
        clientSecret: 'a2c7d198036fa0295d6c2809f492aaf7699dbfe2',
        callbackURL: 'http://deckster.herokuapp.com/auth/github/callback'
      },
      verifyHandler
    ));

    passport.use(new FacebookStrategy(
      {
        clientID: '657423064314660',
        clientSecret: '79591f75c03030f5a4f5070c61de485c',
        callbackURL: 'http://deckster.herokuapp.com/auth/facebook/callback'
      },
      verifyHandler
    ));

    passport.use(new GoogleStrategy(
      {
        clientID: '462299870113-vhceqtbep5d8pjb5n45hlh9g46tfu9eo.apps.googleusercontent.com',
        clientSecret: 'FdmFkQ1FmC2lVl_ImEhvUDTP',
        callbackURL: 'http://deckster.herokuapp.com/auth/google/callback'
      },
      verifyHandler
    ));

    // passport.use(new TwitterStrategy(
    //   {
    //     clientID: '',
    //     clientSecret: '',
    //     callbackURL: '/auth/twitter/callback'
    //   },
    //   verifyHandler
    // ));

    app.use(hsts);
    app.use(passport.initialize());
    app.use(passport.session());
  }
};
