(function () {

  var tabs = document.querySelector('paper-tabs'),
      list = document.querySelector('post-list');

  tabs.addEventListener('core-select', () => {
    list.show = tabs.selected
  });

})();
