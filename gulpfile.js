var gulp = require('gulp');
var esnext = require('gulp-esnext');
var styl = require('gulp-styl');
var inline = require('rework-inline');

gulp.task('default', [ 'scripts', 'styles' ]);

gulp.task('scripts', function () {
  return (
    gulp.src('assets/scripts/**/*js')
      .pipe(esnext())
      .pipe(gulp.dest('assets/dist/scripts'))
  );
});

gulp.task('styles', function () {
  return (
    gulp.src('assets/styles/*.styl')
      .pipe(styl(inline()))
      .pipe(gulp.dest('assets/dist/styles'));
  );
});
